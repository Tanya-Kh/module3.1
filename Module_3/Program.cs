﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Module_3
{
    static class Program
    {
        static void Main(string[] args)
        {
            Task1 task1 = new Task1();
            try
            {
                Console.WriteLine("Please enter a whole number");
                int a = task1.ParseAndValidateIntegerNumber(Console.ReadLine());
                Console.WriteLine("Please enter the second whole number");
                int b = task1.ParseAndValidateIntegerNumber(Console.ReadLine());
                Console.WriteLine("Please see the multiplication result of these two numbers");
                Console.WriteLine(task1.Multiplication(a, b));
            }
            catch (ArgumentException e)
            {
                Console.WriteLine("{0}: {1}", e.GetType().Name, e.Message);
            }

            //task2
            //Task2 task2 = new Task2();

            //Console.WriteLine("Please enter a natural number");
            //if (task2.TryParseNaturalNumber(Console.ReadLine(), out int result))
            //{
            //    Console.WriteLine(String.Format("Please see the first {0} natural numbers.", result));
            //    var numbers = task2.GetEvenNumbers(result);
            //    numbers.ForEach(Console.WriteLine);
            //}
            //else
            //{
            //    Console.WriteLine("Your input is not a natural number! Please try again.");
            //}

            Task3 task3 = new Task3();

            Console.WriteLine("Please enter a natural number");
            if (task3.TryParseNaturalNumber(Console.ReadLine(), out int result))
            {
                Console.WriteLine("Please enter the digit you want to remove");
                int digit = int.Parse(Console.ReadLine());
                Console.WriteLine("Please see new number.");
                Console.WriteLine(task3.RemoveDigitFromNumber(result, digit));
            }
            else
            {
                Console.WriteLine("Your input is not a natural number! Please try again.");
            }
        }
    }
    public class Task1
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// Throw ArgumentException if user input is invalid
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int ParseAndValidateIntegerNumber(string source)
        {
            if (int.TryParse(source, out int a))
            {
                return a;
            }
            else
            {
                throw new ArgumentException(String.Format("{0} is not a whole number", source));
            }
        }

        public int Multiplication(int num1, int num2)
        {
            int sum = 0;
            for (int i = 1; i <= Math.Abs(num2); i++)
            {
                sum += num1;
            }
            return num2 < 0 ? -sum : sum;
        }
    }

    public class Task2
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            if (int.TryParse(input, out result))
            {
                return result >= 0;
            }
            else
            {
                return false;
            }
        }

        public List<int> GetEvenNumbers(int naturalNumber)
        {
            var naturalNumbers = new List<int>();
            int number = 0;

            for (int i = 0; i < naturalNumber; i++)
            {
                naturalNumbers.Add(number);
                number += 2;
            }
            return naturalNumbers;
        }
    }

    public class Task3
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            if (int.TryParse(input, out result))
            {
                return result >= 0;
            }
            else
            {
                return false;
            }
        }

        public string RemoveDigitFromNumber(int source, int digitToRemove)
        {
            string inputString = source.ToString();
            int indexToRemove = inputString.IndexOf(digitToRemove.ToString());
            if (indexToRemove != -1)
            {
                return inputString.Replace(inputString[indexToRemove].ToString(), String.Empty);
            }
            else
            {
                return inputString;
            }
        }
    }
}
